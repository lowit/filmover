from functools import lru_cache
from hashlib import sha3_512

from sqlalchemy import (Column, Float, ForeignKey, Integer, JSON, SmallInteger,
                        String, TIMESTAMP)

from backend import settings
from backend.app import api


@lru_cache(maxsize=1024)
def is_valid_user_token(token: str, password: str) -> bool:
    """Check user token (cached)."""
    return token == sha3_512(
        bytes(str(settings.TOKEN_SALT + password), 'utf-8')
    ).hexdigest()


class User(api.DBModelBase):
    """User model."""

    __tablename__: str = 'users'

    id = Column(Integer, primary_key=True)  # noqa A003
    login = Column(String(64), nullable=False, unique=True)
    password = Column(String(1024), nullable=False)
    email = Column(String(128), nullable=False, unique=True)
    creation = Column(TIMESTAMP)

    def create_password(self, password: str) -> None:
        """
        Hash raw password and set in self.

        :param password: just raw password
        """
        self.password = sha3_512(
            bytes(str(settings.PASSWORD_SALT + password), 'utf-8')
        ).hexdigest()

    def is_valid_token(self, token: str) -> bool:
        """
        Check token.

        :param token: user token
        """
        return is_valid_user_token(token, self.password)

    def is_valid_password(self, password: str) -> bool:
        """
        Check password.

        :param password: Raw user password
        """
        return self.password == sha3_512(
            bytes(str(settings.PASSWORD_SALT + password), 'utf-8')
        ).hexdigest()

    @property  # type: ignore
    @lru_cache()
    def token(self) -> str:
        """Return user token."""
        return sha3_512(
            bytes(str(settings.TOKEN_SALT + self.password), 'utf-8')
        ).hexdigest()


class Movie(api.DBModelBase):
    """Movie model."""

    TYPE_SERIAL: int = 1
    TYPE_FILM: int = 2

    STATUS_WATCHED: int = 1
    STATUS_WATCHING: int = 2
    STATUS_NOT_WATCHED: int = 3

    __tablename__: str = 'movies'

    id = Column(Integer, primary_key=True)  # noqa A003
    id_user = Column(Integer, ForeignKey('users.id'), nullable=False)
    name = Column(String(256), nullable=False)
    year = Column(Integer)
    rating = Column(Float)
    tags = Column(JSON)
    type_movie = Column(SmallInteger, nullable=False)
    status = Column(SmallInteger, nullable=False)
    season_count = Column(SmallInteger)
    season_current = Column(SmallInteger)
    episodes_per_season = Column(SmallInteger)
    episode_current = Column(SmallInteger)
    time_current = Column(Integer)
