from marshmallow import Schema, fields

from backend.app import api


@api.schema('User')
class UserSchema(Schema):
    """Schema for user model (User)."""

    id = fields.Int(dump_only=True)  # noqa A003
    login = fields.Str(required=True)
    email = fields.Email(required=True)
    creation = fields.DateTime(required=True)
