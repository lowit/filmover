from marshmallow import Schema, fields

from backend.app import api


@api.schema('Movie')
class MovieSchema(Schema):
    """Schema for model movie (Movie)."""

    id = fields.Int(dump_only=True)  # noqa A003
    id_user = fields.Int(required=True, dump_only=True)
    name = fields.Str()
    year = fields.Int()
    rating = fields.Float()
    tags = fields.Raw()
    type_movie = fields.Int()
    status = fields.Int()
    season_count = fields.Int(allow_none=True)
    season_current = fields.Int(allow_none=True)
    episodes_per_season = fields.Int(allow_none=True)
    episode_current = fields.Int(allow_none=True)
    time_current = fields.Int(allow_none=True)
