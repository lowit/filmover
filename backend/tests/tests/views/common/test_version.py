class TestVersion:
    """Testing view version."""

    URL = '/version'
    VERSION = '7.7.7'

    def test_get_version(self, api, user):
        """Testing api with valid user and valid token."""
        api.version = self.VERSION

        resp = api.requests.get(self.URL)

        assert resp.status_code == 200
        assert resp.json()['message'] == self.VERSION

    def test_invalid_methods(self, api, user):
        """Testing api with not supported methods."""
        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.delete(self.URL, headers=headers)
        assert resp.status_code == 405

        resp = api.requests.post(self.URL, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405

        resp = api.requests.put(self.URL, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405
