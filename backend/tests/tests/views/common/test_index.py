import pytest
from requests.exceptions import ConnectionError

from backend import settings


class TestIndexSPA:
    """Testing view index."""

    URL = '/'

    def test_200(self, api, monkeypatch):
        """Testing valid returning status code."""
        api.template = lambda x: x

        resp = api.requests.get(self.URL)

        assert resp.status_code == 200

    def test_invalid_methods(self, api, user):
        """Testing api with not supported methods."""
        resp = api.requests.delete(self.URL)
        assert resp.status_code == 405

        resp = api.requests.post(self.URL, data={'t': 't'})
        assert resp.status_code == 405

        resp = api.requests.put(self.URL, data={'t': 't'})
        assert resp.status_code == 405

    def test_api_with_debug(self, api, user):
        """
        Testing api with setting DEBUG=True.

        ToDo: rewrite with request mock
        """
        settings.DEBUG = True
        with pytest.raises(ConnectionError):
            api.requests.get(self.URL)
