import pytest


class TestMessageAndStatus:
    """Testing default views/message_and_status function."""

    @pytest.mark.parametrize('url', (
        '/da3gi6if3dt5aiFT1BAFib6mv5at32gIEft66',
    ))
    def test_not_found_url(self, url, api):
        """Testing response with bad url."""
        resp = api.requests.get(url=url)

        assert resp.status_code == 404
        assert resp.json()['message'] == 'Not found'
        assert resp.json()['status'] is False
