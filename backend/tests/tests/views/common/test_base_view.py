from backend.views.common import BaseViewed


class MockResponse:
    """Mock response for tests."""

    status_code = 200
    media = None


class TestBaseView:
    """Test methods in base view."""

    TEST_VIEW = BaseViewed()
    COMMON_REQUEST = type('MockRequest', (object,), {})

    def test_code_method_not_allowed(self):
        """Test code for method not allowed."""
        response = MockResponse()
        self.TEST_VIEW.get_data(self.COMMON_REQUEST, response)
        assert response.status_code == 405

    def test_media_method_not_allowed(self):
        """Test media for method not allowed."""
        response = MockResponse()
        self.TEST_VIEW.get_data(self.COMMON_REQUEST, response)
        assert response.media == {
            'message': 'Method not allowed',
            'status': False
        }
