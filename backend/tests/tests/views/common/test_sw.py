class TestServiceWorker:
    """Testing view for service worker."""

    URL = '/sw.js'

    def test_200(self, api):
        """Testing valid returning status code."""
        api.template = lambda x: x

        resp = api.requests.get(self.URL)

        assert resp.status_code == 200

    def test_invalid_methods(self, api, user):
        """Testing api with not supported methods."""
        resp = api.requests.delete(self.URL)
        assert resp.status_code == 405

        resp = api.requests.post(self.URL, data={'t': 't'})
        assert resp.status_code == 405

        resp = api.requests.put(self.URL, data={'t': 't'})
        assert resp.status_code == 405
