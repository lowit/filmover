import pytest
from requests.exceptions import ConnectionError

from backend import settings


class TestDebugSpa:
    """Testing view debug spa."""

    URL = '/_nuxt/test/path/'

    def test_with_debug(self, api):
        """
        Testing api with setting DEBUG=True.

        ToDo: rewrite with request mock
        """
        settings.DEBUG = True
        with pytest.raises(ConnectionError):
            api.requests.get(self.URL)

    def test_without_debug_404(self, api):
        """Testing api with setting DEBUG=False."""
        settings.DEBUG = False
        request = api.requests.get(self.URL)
        assert request.status_code == 404

    def test_invalid_methods(self, api, user):
        """Testing api with not supported methods."""
        resp = api.requests.delete(self.URL)
        assert resp.status_code == 405

        resp = api.requests.post(self.URL, data={'t': 't'})
        assert resp.status_code == 405

        resp = api.requests.put(self.URL, data={'t': 't'})
        assert resp.status_code == 405
