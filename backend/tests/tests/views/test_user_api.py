import mock

from backend import settings
from backend.models import User


class TestGetUser:
    """Testing get_user view."""

    USER_TOKEN = 'ed54f56ef49803011fef17c18fd355d64fae8a7f1ab400025108cd179' \
                 '166a50233cc9ff58405529ffa8667bcdaa557ea88029eddabbf2b4ce7' \
                 'aaadf211c88df1'

    def test_reject_digital_password(self, api):
        """Test forbidden with digital password."""
        url = f'/api/v1/users/1'
        headers = {'X-Password': '111', 'Cookie': ''}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 403
        assert resp.json()['message'] == 'Not allowed'
        assert resp.json()['status'] is False

    def test_valid_id_and_password(self, user, api):
        """Test request with valid id and password."""
        url = f'/api/v1/users/{user.id}'
        headers = {'X-Password': 'admin'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 200
        assert resp.json()['id'] == user.id
        assert resp.json()['login'] == user.login
        assert resp.json()['email'] == user.email
        assert resp.json()['creation'] == user.creation.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00')

        token_value = '; '.join(
            [self.USER_TOKEN, *settings.PARAMS_FOR_TOKEN_COOKIE])
        assert resp.headers['Set-Cookie'] == f'token={token_value};'

    def test_valid_id_and_token(self, user, api):
        """Test request with valid id and token."""
        url = f'/api/v1/users/{user.id}'
        headers = {'Cookie': f'token={self.USER_TOKEN}'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 200
        assert resp.json()['id'] == user.id
        assert resp.json()['login'] == user.login
        assert resp.json()['email'] == user.email
        assert resp.json()['creation'] == user.creation.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00')
        assert resp.headers.get('Set-Cookie') is None

    def test_valid_login_and_password(self, user, api):
        """Test request with valid login and password."""
        url = f'/api/v1/users/{user.login}'
        headers = {'X-Password': 'admin'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 200
        assert resp.json()['id'] == user.id
        assert resp.json()['login'] == user.login
        assert resp.json()['email'] == user.email
        assert resp.json()['creation'] == user.creation.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00')

        token_value = '; '.join(
            [self.USER_TOKEN, *settings.PARAMS_FOR_TOKEN_COOKIE])
        assert resp.headers['Set-Cookie'] == f'token={token_value};'

    def test_valid_login_and_token(self, user, api):
        """Test request with valid login and password."""
        url = f'/api/v1/users/{user.login}'
        headers = {'Cookie': f'token={self.USER_TOKEN}'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 200
        assert resp.json()['id'] == user.id
        assert resp.json()['login'] == user.login
        assert resp.json()['email'] == user.email
        assert resp.json()['creation'] == user.creation.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00')
        assert resp.headers.get('Set-Cookie') is None

    def test_empty_password_and_token(self, api):
        """Test request without token and password."""
        url = f'/api/v1/users/none_user'
        headers = {'Cookie': ''}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 403
        assert resp.json()['message'] == 'Not allowed'
        assert resp.json()['status'] is False

    def test_invalid_login(self, api, user):
        """Test request with invalid login."""
        url = f'/api/v1/users/{user.login}_bad_login'
        headers = {'Cookie': f'token=bad_token'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 403
        assert resp.json()['message'] == 'Not allowed'
        assert resp.json()['status'] is False

    def test_valid_id_and_invalid_password(self, api, user):
        """Test request with valid id and invalid password."""
        url = f'/api/v1/users/{user.id}'
        headers = {'X-Password': 'bad_password', 'Cookie': ''}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 403
        assert resp.json()['message'] == 'Not allowed'
        assert resp.json()['status'] is False

    def test_valid_id_and_invalid_token(self, api, user):
        """Test request with valid id and invalid token."""
        url = f'/api/v1/users/{user.id}'
        headers = {'Cookie': f'token=bad_token'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 403
        assert resp.json()['message'] == 'Not allowed'
        assert resp.json()['status'] is False

    def test_valid_login_and_invalid_password(self, api, user):
        """Test request with valid login and invalid password."""
        url = f'/api/v1/users/{user.login}'
        headers = {'X-Password': 'bad_password', 'Cookie': ''}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 403
        assert resp.json()['message'] == 'Not allowed'
        assert resp.json()['status'] is False

    def test_valid_login_and_invalid_token(self, api, user):
        """Test request with valid login and invalid token."""
        url = f'/api/v1/users/{user.login}'
        headers = {'Cookie': f'token=bad_token'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 403
        assert resp.json()['message'] == 'Not allowed'
        assert resp.json()['status'] is False

    def test_valid_login_valid_passwd_and_invalid_token(self, api, user):
        """Test request with valid login, valid password and invalid token."""
        url = f'/api/v1/users/{user.login}'
        headers = {'X-Password': 'admin', 'Cookie': f'token=bad_token'}

        resp = api.requests.get(url, headers=headers)

        assert resp.status_code == 200
        assert resp.json()['id'] == user.id
        assert resp.json()['login'] == user.login
        assert resp.json()['email'] == user.email
        assert resp.json()['creation'] == user.creation.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00')

        token_value = '; '.join(
            [self.USER_TOKEN, *settings.PARAMS_FOR_TOKEN_COOKIE])
        assert resp.headers['Set-Cookie'] == f'token={token_value};'

    def test_invalid_methods(self, api, user):
        """Testing api with not supported methods."""
        url = f'/api/v1/users/{user.login}'

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.delete(url, headers=headers)
        assert resp.status_code == 405

        resp = api.requests.post(url, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405

        resp = api.requests.put(url, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405


class TestGoogleAuth:
    """Testing google auth view."""

    URL = '/api/v1/authwithgoogle'
    HEADERS = {
        'X-Requested-With': 'XMLHttpRequest',
        'X-Google-Auth-Code': '000'
    }

    def test_csrf_protect(self, api):
        """Test csrf protection."""
        resp = api.requests.get(self.URL)
        assert resp.status_code == 403

    def test_google_error(self, api):
        """Test error response."""
        resp = api.requests.get(self.URL, headers=self.HEADERS)
        assert resp.status_code == 403

    @mock.patch('backend.views.user.client.'
                'credentials_from_clientsecrets_and_code')
    def test_user_already_registered(self, client, user, api):
        """Test auth with already registered user."""
        credentials = type('Credentials', (), {
            'id_token': {'sub': 100, 'email': user.email}
        })
        client.return_value = credentials

        user.login = user.email
        api.db_session.flush()

        resp = api.requests.get(self.URL, headers=self.HEADERS)

        assert resp.status_code == 200
        assert resp.json()['id'] == user.id
        assert resp.json()['login'] == user.login
        assert resp.json()['email'] == user.email
        assert resp.json()['creation'] == user.creation.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00')

        token_value = '; '.join(
            [user.token, *settings.PARAMS_FOR_TOKEN_COOKIE])
        assert resp.headers['Set-Cookie'] == f'token={token_value};'

    @mock.patch('backend.views.user.client.'
                'credentials_from_clientsecrets_and_code')
    def test_new_user(self, client, user, api):
        """Test auth with new user."""
        new_user_email = 'newuser@filmover.ru'
        credentials = type('Credentials', (), {
            'id_token': {'sub': 100, 'email': new_user_email}
        })
        client.return_value = credentials

        resp = api.requests.get(self.URL, headers=self.HEADERS)
        user = api.db_session.query(User).get(resp.json()['id'])

        assert resp.status_code == 200
        assert resp.json()['login'] == new_user_email == user.login
        assert resp.json()['email'] == new_user_email == user.email

        token_value = '; '.join(
            [user.token, *settings.PARAMS_FOR_TOKEN_COOKIE])
        assert resp.headers['Set-Cookie'] == f'token={token_value};'
