from backend import models


class TestMovieApiDeleteOne:
    """
    Testing api movies delete one.

    url: /api/v1/movies/{id_movie}
    """

    def test_with_valid_user_and_token(
            self, api, user, movies_default_user, movies_other, default_movie
    ):
        """Testing api with valid user and valid token."""
        movie_id = default_movie.id

        movie_for_delete = api.db_session.query(
            models.Movie
        ).get(movie_id)

        url = f'/api/v1/movies/{movie_for_delete.id}'
        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.delete(url, headers=headers)

        assert resp.status_code == 200
        assert resp.json()['status'] is True
        assert resp.json()['message'] == 'Movie deleted successfully'

        deleted_movie = api.db_session.query(
            models.Movie
        ).filter(
            models.Movie.id == movie_id
        ).first()

        assert deleted_movie is None

    def test_with_valid_user_and_invalid_token(
            self, api, user, movies_default_user, movies_other, default_movie
    ):
        """Testing api with valid user and invalid token."""
        url = f'/api/v1/movies/{default_movie.id}'
        headers = {
            'Cookie': f'token=bad_token',
            'X-User': str(user.id)
        }

        resp = api.requests.get(url, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.delete(url, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.put(url, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.post(url, headers=headers)
        assert resp.status_code == 403

    def test_not_found_movie_in_db(
            self, api, user, movies_default_user,
            movies_other, default_movie, user_with_empty_movies
    ):
        """Testing api with empty return result for database request."""
        url = f'/api/v1/movies/{default_movie.id}'
        headers = {
            'Cookie': f'token={user_with_empty_movies.token}',
            'X-User': str(user_with_empty_movies.id)
        }

        resp = api.requests.delete(url, headers=headers)

        assert resp.status_code == 404

    def test_invalid_methods(self, api, user, default_movie):
        """Testing api with not supported methods."""
        url = f'/api/v1/movies/{default_movie.id}'

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.post(url, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405
