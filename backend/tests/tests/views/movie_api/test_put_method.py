import json

from backend import models


class TestMovieApiEdit:
    """
    Testing api add movie.

    url: /api/v1/movies/
    """

    def test_with_valid_user_and_token(
            self, api, user, movies_default_user, movies_other, default_movie
    ):
        """Testing api with valid user and valid token."""
        user_id = user.id
        default_movie_id = default_movie.id

        url = f'/api/v1/movies/{default_movie.id}'

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user_id)
        }
        data = """
        {
          "season_count": 777,
          "time_current": 777,
          "name": "updated_name",
          "tags": [
            "update_tag1",
            "update_tag2",
            "update_tag3"
          ],
          "episodes_per_season": 777,
          "episode_current": 777,
          "season_current": 777,
          "rating": 7.7,
          "year": 2077,
          "status": 3,
          "type_movie": 2
        }
        """

        resp = api.requests.put(url, data=data, headers=headers)

        data = json.loads(data)

        assert resp.status_code == 200
        assert resp.json()['status'] is True
        assert resp.json()['message'] == 'Movie updated successfully'

        updated_movie = api.db_session.query(
            models.Movie
        ).get(default_movie_id)

        for key, value in data.items():
            assert getattr(updated_movie, key) == value

        assert updated_movie.id_user == user_id

    def test_with_invalid_movie_id(
        self, api, user, movies_default_user, movies_other, default_movie
    ):
        """Testing api with bad movie id."""
        url = f'/api/v1/movies/777'

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.put(url, data={'name': 'test'}, headers=headers)

        assert resp.status_code == 404

    def test_with_valid_user_and_invalid_token(
            self, api, user, movies_default_user, movies_other, default_movie
    ):
        """Testing api with valid user and invalid token."""
        url = f'/api/v1/movies/{default_movie.id}'

        headers = {
            'Cookie': f'token=bad_token',
            'X-User': str(user.id)
        }

        resp = api.requests.get(url, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.delete(url, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.put(url, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.post(url, headers=headers)
        assert resp.status_code == 403

    def test_invalid_methods(self, api, user, default_movie):
        """Testing api with not supported methods."""
        url = f'/api/v1/movies/{default_movie.id}'

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.post(url, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405
