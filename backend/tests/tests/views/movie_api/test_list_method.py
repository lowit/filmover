import pytest

from backend import models


class TestMovieApiList:
    """
    Testing api movies list.

    url: /api/v1/movies/
    func: list_movies
    """

    URL = '/api/v1/movies/'

    def test_with_valid_user_and_token(
            self, api, user, movies_default_user, movies_other
    ):
        """Testing api with valid user and valid token."""
        valid_movies_id = {movie.id for movie in movies_default_user}

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.get(self.URL, headers=headers)

        response_movies_id = {movie['id'] for movie in resp.json()}

        assert resp.status_code == 200
        assert len(resp.json()) == len(valid_movies_id)
        assert response_movies_id == valid_movies_id

    @pytest.mark.parametrize('type_movie', (
        models.Movie.TYPE_FILM, models.Movie.TYPE_SERIAL
    ))
    def test_with_filter_type(self, type_movie, api, user, movies_all_types):
        """Testing api with filter param type."""
        valid_movies_id = {movie.id for movie in filter(
            lambda movie: movie.type_movie == type_movie,
            movies_all_types
        )}

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        params = {'type': type_movie}

        resp = api.requests.get(self.URL, params=params, headers=headers)

        response_movies_id = {movie['id'] for movie in resp.json()}

        assert resp.status_code == 200
        assert len(resp.json()) == len(valid_movies_id)
        assert response_movies_id == valid_movies_id

    @pytest.mark.parametrize('status', (
        models.Movie.STATUS_WATCHING,
        models.Movie.STATUS_WATCHED,
        models.Movie.STATUS_NOT_WATCHED,
    ))
    def test_with_filter_status(self, status, api, user, movies_all_types):
        """Testing api with filter param status."""
        valid_movies_id = {movie.id for movie in filter(
            lambda movie: movie.status == status,
            movies_all_types
        )}

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        params = {'status': status}

        resp = api.requests.get(self.URL, params=params, headers=headers)

        response_movies_id = {movie['id'] for movie in resp.json()}

        assert resp.status_code == 200
        assert len(resp.json()) == len(valid_movies_id)
        assert response_movies_id == valid_movies_id

    @pytest.mark.parametrize('status, type_movie', (
        (models.Movie.STATUS_WATCHING, models.Movie.TYPE_FILM,),
        (models.Movie.STATUS_WATCHED, models.Movie.TYPE_FILM,),
        (models.Movie.STATUS_NOT_WATCHED, models.Movie.TYPE_FILM,),
        (models.Movie.STATUS_WATCHING, models.Movie.TYPE_SERIAL,),
        (models.Movie.STATUS_WATCHED, models.Movie.TYPE_SERIAL,),
        (models.Movie.STATUS_NOT_WATCHED, models.Movie.TYPE_SERIAL,),
    ))
    def test_with_filter_status_and_type(
            self, status, type_movie, api, user, movies_all_types
    ):
        """Testing api with filter param status and type."""
        valid_movies_id = {movie.id for movie in filter(
            lambda movie:
            movie.status == status and movie.type_movie == type_movie,
            movies_all_types
        )}

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        params = {'status': status, 'type': type_movie}

        resp = api.requests.get(self.URL, params=params, headers=headers)

        response_movies_id = {movie['id'] for movie in resp.json()}

        assert resp.status_code == 200
        assert len(resp.json()) == len(valid_movies_id)
        assert response_movies_id == valid_movies_id

    def test_with_valid_user_and_invalid_token(
            self, api, user, movies_default_user, movies_other
    ):
        """Testing api with valid user and invalid token."""
        headers = {
            'Cookie': f'token=bad_token',
            'X-User': str(user.id)
        }

        resp = api.requests.get(self.URL, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.delete(self.URL, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.put(self.URL, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.post(self.URL, headers=headers)
        assert resp.status_code == 403

    def test_empty_list_movies_in_db(
            self, api, user, movies_default_user,
            movies_other, user_with_empty_movies
    ):
        """Testing api with empty return result for database request."""
        headers = {
            'Cookie': f'token={user_with_empty_movies.token}',
            'X-User': str(user_with_empty_movies.id)
        }

        resp = api.requests.get(self.URL, headers=headers)

        assert resp.status_code == 404

    def test_invalid_methods(self, api, user):
        """Testing api with not supported methods."""
        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.delete(self.URL, headers=headers)
        assert resp.status_code == 405

        resp = api.requests.put(self.URL, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405
