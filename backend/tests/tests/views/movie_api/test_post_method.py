import json

from backend import models


class TestMovieApiAdd:
    """
    Testing api add movie.

    url: /api/v1/movies/
    """

    URL = '/api/v1/movies/'

    def test_with_valid_user_and_token(
            self, api, user, movies_default_user, movies_other
    ):
        """Testing api with valid user and valid token."""
        user_id = user.id

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user_id)
        }
        data = """
        {
          "season_count": 6,
          "time_current": 123123,
          "name": "test1",
          "tags": [
            "tag1",
            "tag2",
            "tag3"
          ],
          "episodes_per_season": 12,
          "episode_current": 1,
          "season_current": 2,
          "rating": 7.8,
          "year": 2010,
          "status": 1,
          "type_movie": 1
        }
        """

        resp = api.requests.post(self.URL, data=data, headers=headers)

        data = json.loads(data)

        assert resp.status_code == 201
        assert resp.json()['status'] is True

        new_movie = api.db_session.query(
            models.Movie
        ).get(int(resp.json()['message']))

        for key, value in data.items():
            assert getattr(new_movie, key) == value

        assert new_movie.id_user == user_id

    def test_with_valid_user_and_invalid_token(
            self, api, user, movies_default_user, movies_other
    ):
        """Testing api with valid user and invalid token."""
        headers = {
            'Cookie': f'token=bad_token',
            'X-User': str(user.id)
        }

        resp = api.requests.get(self.URL, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.delete(self.URL, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.put(self.URL, headers=headers)
        assert resp.status_code == 403

        resp = api.requests.post(self.URL, headers=headers)
        assert resp.status_code == 403

    def test_invalid_methods(self, api, user):
        """Testing api with not supported methods."""
        url = '/api/v1/movies/'

        headers = {
            'Cookie': f'token={user.token}',
            'X-User': str(user.id)
        }

        resp = api.requests.delete(url, headers=headers)
        assert resp.status_code == 405

        resp = api.requests.put(url, data={'t': 't'}, headers=headers)
        assert resp.status_code == 405
