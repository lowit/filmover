from mock import mock_open, patch

from backend.settings import get_version_from_file


class TestGetVersionFromFile:
    """Test get_version_from_file func."""

    @patch('builtins.open', new_callable=mock_open, read_data='')
    def test_except(self, mock_file):
        """Test exception in func."""
        assert get_version_from_file() == '0.0.0'
