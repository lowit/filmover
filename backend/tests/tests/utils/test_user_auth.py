from backend.utils import user_auth


class TestUserAuth:
    """Testing user_auth."""

    USER_PASSWD = 'admin'

    def test_empty_token_and_password(self):
        """Testing call function without token and password."""
        assert user_auth(id_user=1) is False

    def test_empty_user_and_user_id(self):
        """Testing call function without user and user_id."""
        assert user_auth(password='1') is False

    def test_invalid_user_login(self, api):
        """Testing call function with invalid user login."""
        assert user_auth(id_user='bad_user', password='1') is False

    def test_invalid_user_id(self, api):
        """Testing call function with invalid user id."""
        assert user_auth(id_user=777, password='1') is False

    def test_valid_user_login_and_invalid_password(self, user):
        """Testing call function with valid user login and invalid password."""
        assert user_auth(id_user=user.login, password='1') is False

    def test_valid_user_id_and_invalid_password(self, user):
        """Testing call function with valid user id and invalid password."""
        assert user_auth(id_user=user.id, password='1') is False

    def test_valid_user_login_and_invalid_token(self, user):
        """Testing call function with valid user login and invalid token."""
        assert user_auth(id_user=user.login, token='1') is False

    def test_valid_user_id_and_invalid_token(self, user):
        """Testing call function with valid user id and invalid token."""
        assert user_auth(id_user=user.id, token='1') is False

    def test_valid_user_login_and_valid_token(self, user):
        """Testing call function with valid user login and valid token."""
        assert user_auth(id_user=user.login, token=user.token) is True

    def test_valid_user_id_and_valid_token(self, user):
        """Testing call function with valid user id and valid token."""
        assert user_auth(id_user=user.id, token=user.token) is True

    def test_valid_user_login_and_valid_password(self, user):
        """Testing call function with valid user login and valid password."""
        assert user_auth(id_user=user.login, password=self.USER_PASSWD) is True

    def test_valid_user_id_and_valid_password(self, user):
        """Testing call function with valid user id and valid password."""
        assert user_auth(id_user=user.id, password=self.USER_PASSWD) is True

    def test_object_user_and_invalid_token(self, user):
        """Testing call function with object user and invalid token."""
        assert user_auth(user=user, token='1') is False

    def test_object_user_and_invalid_password(self, user):
        """Testing call function with object user and invalid password."""
        assert user_auth(user=user, token='1') is False

    def test_object_user_and_valid_token(self, user):
        """Testing call function with object user and valid token."""
        assert user_auth(user=user, token=user.token) is True

    def test_object_user_and_valid_password(self, user):
        """Testing call function with object user and valid password."""
        assert user_auth(user=user, password=self.USER_PASSWD) is True
