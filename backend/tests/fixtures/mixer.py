from datetime import datetime as dt

from mixer.backend.sqlalchemy import Mixer


class Xyuxer(Mixer):
    """Custom Mixer."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        from backend import models
        self.models = models

    # model.User fixtures
    def blend_user(self, **kwargs):
        """Return just user object."""
        return self.blend(self.models.User, **kwargs)

    def blend_default_user(self):
        """Return admin user."""
        return self.blend_user(
            login='admin',
            password='c91727c088f36a9e8ad411724b7cee0d5e50687b482'
                     'd5e20e61dbff23bb3993a1c8c4bc89965fd37d5aa2d'
                     '75169cdc12d8b7a27aebb8ffaeadb6bd09b97bedda',
            email='ad@min.ru',
            creation=dt(
                year=2012, month=12, day=12, hour=12, minute=12, second=12
            )
        )
    # model.User fixtures end

    # model.Movie fixtures
    def blend_movie(self, **kwargs):
        """Return just movie."""
        return self.blend(self.models.Movie, **kwargs)

    def blend_default_serial_unwatch(self, **kwargs):
        """Return unwatch serial."""
        return self.blend_movie(
            type_movie=self.models.Movie.TYPE_SERIAL,
            status=self.models.Movie.STATUS_NOT_WATCHED,
            **kwargs
        )

    def blend_default_film_unwatch(self, **kwargs):
        """Return unwatch film."""
        return self.blend_movie(
            type_movie=self.models.Movie.TYPE_FILM,
            status=self.models.Movie.STATUS_NOT_WATCHED,
            **kwargs
        )

    def blend_default_serial_watching(self, **kwargs):
        """Return watching serial."""
        return self.blend_movie(
            type_movie=self.models.Movie.TYPE_SERIAL,
            status=self.models.Movie.STATUS_WATCHING,
            **kwargs
        )

    def blend_default_film_watching(self, **kwargs):
        """Return watching film."""
        return self.blend_movie(
            type_movie=self.models.Movie.TYPE_FILM,
            status=self.models.Movie.STATUS_WATCHING,
            **kwargs
        )

    def blend_default_serial_watched(self, **kwargs):
        """Return watched serial."""
        return self.blend_movie(
            type_movie=self.models.Movie.TYPE_SERIAL,
            status=self.models.Movie.STATUS_WATCHED,
            **kwargs
        )

    def blend_default_film_watched(self, **kwargs):
        """Return watched film."""
        return self.blend_movie(
            type_movie=self.models.Movie.TYPE_FILM,
            status=self.models.Movie.STATUS_WATCHED,
            **kwargs
        )
    # model.Movie fixtures end
