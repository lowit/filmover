import pytest

from backend import models


@pytest.fixture(scope='function')
def user(request, api, mixer):
    """User fixture return User object."""
    return mixer.blend_default_user()


@pytest.fixture(scope='function')
def user_with_empty_movies(request, api, mixer):
    """User fixture return User object with empty movies."""
    return mixer.blend_user(
        login='user_with_empty_movies',
        password='user_with_empty_movies',
        email='user_with_empty_@movi.es'
    )


@pytest.fixture(scope='function')
def default_movie(request, api, mixer, user):
    """Movie fixture return object movie for default user."""
    return mixer.blend_default_serial_unwatch(
        id_user=user.id,
        name='default_movie',
    )


@pytest.fixture(scope='function')
def movies_default_user(request, api, mixer, user):
    """Movies fixture return list objects movies for default user."""
    movies_list = [
        mixer.blend_default_serial_unwatch(
            id_user=user.id,
            name='serial_unwatch_default_user',
        ),
        mixer.blend_default_film_unwatch(
            id_user=user.id,
            name='film_unwatch_default_user',
        ),
        mixer.blend_default_serial_watching(
            id_user=user.id,
            name='serial_watch_default_user',
        ),
        mixer.blend_default_film_watching(
            id_user=user.id,
            name='film_watch_default_user',
        ),
        mixer.blend_default_serial_watched(
            id_user=user.id,
            name='serial_watched_default_user',
        ),
        mixer.blend_default_film_watched(
            id_user=user.id,
            name='film_watched_default_user',
        ),
    ]

    return movies_list


@pytest.fixture(scope='function')
def movies_other(request, api, mixer):
    """Movies fixture return list objects movies."""
    user2 = mixer.blend_user(login='user2', password='user2', email='u2@se.r')

    movies_list = [
        mixer.blend_default_serial_unwatch(
            id_user=user2.id,
            name='serial_unwatch_user2',
        ),
        mixer.blend_default_film_unwatch(
            id_user=user2.id,
            name='film_unwatch_user2',
        ),
        mixer.blend_default_serial_watching(
            id_user=user2.id,
            name='serial_watch_user2',
        ),
        mixer.blend_default_film_watching(
            id_user=user2.id,
            name='film_watch_user2',
        ),
        mixer.blend_default_serial_watched(
            id_user=user2.id,
            name='serial_watched_user2',
        ),
        mixer.blend_default_film_watched(
            id_user=user2.id,
            name='film_watched_user2',
        ),
    ]

    return movies_list


@pytest.fixture(scope='function')
def movies_all_types(request, api, mixer, user):
    """Movies fixture return list all types movies."""
    movies_list = []

    # blend serials and films
    for movie_type in (models.Movie.TYPE_SERIAL, models.Movie.TYPE_FILM):
        for status in (models.Movie.STATUS_NOT_WATCHED,
                       models.Movie.STATUS_WATCHED,
                       models.Movie.STATUS_WATCHING):
            for i in range(1, 10):
                movies_list.append(
                    mixer.blend_movie(
                        name=f'serial{i}',
                        status=status,
                        type_movie=movie_type,
                        tags=['tag1', 'tag2', 'tag3'],
                        id_user=user.id,
                        episode_current=None,
                        rating=7.8,
                        season_count=10,
                        episodes_per_season=12,
                        season_current=None,
                        year=2010,
                        time_current=None,
                    )
                )

    return movies_list
