from os import environ, path


def get_version_from_file() -> str:
    """Return version from VERSION file or fake version if file not found."""
    try:
        with open('VERSION') as ver_file:
            version: str = ver_file.readlines()[0]
            version = version.strip()
    except (FileNotFoundError, IndexError):
        version = '0.0.0'
    return version


TITLE: str = 'api from filmover'
VERSION: str = get_version_from_file()

DOCS_ROUTE: str = '/api/docs'
OPENAPI: str = '3.0.0'

PROJECT_DIR: str = path.dirname(path.dirname(__file__))

DEBUG: bool = False

TEMPLATES_DIR: str = path.join(PROJECT_DIR, 'frontend/dist')
STATIC_DIR: str = path.join(PROJECT_DIR, 'frontend/dist/_nuxt')
STATIC_ROUTE: str = '/_nuxt'

CORS: bool = True
CORS_PARAMS: dict = {
    'allow_credentials': True,
    'allow_headers': ('*',),
    'allow_methods': ('GET', 'POST', 'PUT', 'DELETE'),
    'allow_origin_regex': None,
    'allow_origins': ('filmover.ru',),
    'expose_headers': (),
    'max_age': 600
}

ENABLE_HSTS: bool = False
ALLOWED_HOSTS: tuple = ('filmover.ru', 'next.filmover.ru')
AUTO_ESCAPE: bool = True


SECRET_KEY: str = '__SECRET__'
PASSWORD_SALT: str = '__SALT__'
TOKEN_SALT: str = '__SALT__'

GOOGLE_CLIENT_SECRET_FILE: str = 'secrets/client_secret.json'

PARAMS_FOR_TOKEN_COOKIE: tuple = ('HttpOnly', 'Path=/', 'Max-Age=7948800')

DB_URL_TEMPLATE: str = ('postgresql://{login}:{password}@{host}:{port}/'
                        '{db_name}')

DB_CONF: dict = {
    'login': '__EMPTY__',
    'password': '__EMPTY__',
    'host': '__EMPTY__',
    'port': 0,
    'db_name': '__EMPTY__',
}
DB_URL: str = DB_URL_TEMPLATE.format(**DB_CONF)

DB_PARAMS: dict = {}

DEBUG_FRONT_PORT: int = 3000


if environ.get('PROFILE') == 'local':
    from backend.profiles.local import *  # noqa
if environ.get('PROFILE') == 'release':
    from backend.profiles.release import *  # noqa

if environ.get('PROFILE') == 'test':
    from backend.profiles.test import *  # noqa
else:
    try:
        from .settings_local import *  # noqa
    except ImportError:
        pass
