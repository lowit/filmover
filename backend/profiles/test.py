from os import environ as e, path

PROJECT_DIR = path.dirname(path.dirname(path.dirname(__file__)))

TEMPLATES_DIR = path.join(PROJECT_DIR, 'frontend/dist')
STATIC_DIR = path.join(PROJECT_DIR, 'frontend/dist/_nuxt')

ENABLE_HSTS = False
ALLOWED_HOSTS = ('*',)

# disable for TestDebugSpa test
STATIC_ROUTE = '/disable_for_test'

DB_URL_TEMPLATE: str = ('postgresql://{login}:{password}@{host}:{port}/'
                        '{db_name}')

DB_CONF = {
    'login': 'test_filmover',
    'password': 'test_filmover',
    'host': e.get('TEST_DB_HOST', '127.0.0.1'),
    'port': '5432',
    'db_name': 'test_filmover',
}

DB_URL: str = DB_URL_TEMPLATE.format(**DB_CONF)

DB_PARAMS = {'encoding': 'utf-8'}
