CORS = True
CORS_PARAMS = {
    'allow_credentials': True,
    'allow_headers': ('*',),
    'allow_methods': ('*',),
    'allow_origin_regex': None,
    'allow_origins': ('http://localhost:3000',),
    'expose_headers': (),
    'max_age': 60 * 60 * 24 * 7  # 7 days
}

DB_URL_TEMPLATE: str = ('postgresql://{login}:{password}@{host}:{port}/'
                        '{db_name}')

DB_CONF = {
    'login': 'filmover',
    'password': 'filmover',
    'host': '127.0.0.1',
    'port': '5432',
    'db_name': 'filmover',
}
DB_URL: str = DB_URL_TEMPLATE.format(**DB_CONF)

DB_PARAMS = {'encoding': 'utf-8'}

STATIC_ROUTE = '/disable_static'

ENABLE_HSTS = False
ALLOWED_HOSTS = ('*',)

DEBUG = True
