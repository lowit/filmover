import os

SECRET_KEY = os.environ['SECRET_KEY']

PASSWORD_SALT = os.environ['PASSWORD_SALT']
TOKEN_SALT = os.environ['TOKEN_SALT']

PARAMS_FOR_TOKEN_COOKIE = ('HttpOnly', 'Path=/', 'Max-Age=7948800', 'Secure')

DB_URL_TEMPLATE: str = ('postgresql://{login}:{password}@{host}:{port}/'
                        '{db_name}')

DB_CONF = {
    'login': 'filmover',
    'password': 'filmover',
    'host': 'filmover-db',
    'port': '5432',
    'db_name': 'filmover',
}
DB_URL: str = DB_URL_TEMPLATE.format(**DB_CONF)

DB_PARAMS = {'encoding': 'utf-8'}

CORS = True
CORS_PARAMS = {
    'allow_credentials': True,
    'allow_headers': ('*',),
    'allow_methods': ('GET', 'POST', 'PUT', 'DELETE'),
    'allow_origin_regex': None,
    'allow_origins': ('https://filmover.ru', 'https://next.filmover.ru'),
    'expose_headers': (),
    'max_age': 600
}
