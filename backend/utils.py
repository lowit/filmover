from typing import Optional, Union

from sqlalchemy.orm.query import Query

from backend import models
from .app import api


def user_auth(user: Optional['models.User'] = None,
              id_user: Optional[Union[int, str]] = None,
              token: Optional[str] = None,
              password: Optional[str] = None) -> bool:
    """
    Check user auth.

    :param user: object models.User
    :param password: user password
    :param token: user token
    :param id_user: user identificator - login or id
    """
    if token is None and password is None:
        return False

    if user is None and id_user is None:
        return False

    if user is None:
        user_query: Query = api.db_session.query(
            models.User
        )

        try:
            id_user = int(id_user)  # type: ignore
        except ValueError:
            user = user_query.filter(
                models.User.login == id_user
            ).first()
        else:
            user = user_query.get(id_user)

        if user is None:
            return False

    if token and user.is_valid_token(token):
        return True

    if password and user.is_valid_password(password):
        return True

    return False


api.user_auth = user_auth
