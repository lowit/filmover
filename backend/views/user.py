from datetime import datetime
from typing import Union

from oauth2client import client
from responder.models import Request, Response
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm.query import Query

from backend import models, settings
from backend.app import api
from backend.schemas.user import UserSchema


@api.route('/api/v1/users/{id_user}')
class GetUser(api.BaseViewed):
    r"""
    Get user information and set token in cookie if valid request.

    Access:
    valid token in cookie 'token' or valid password in header 'X-Password'

    :param id_user: user identificator - login or id

    ---
    get:
        description: Get user information and set token \
                     in cookie if valid request
        parameters: [
            {
                'in': 'headers',
                'name': 'X-User',
                'schema': {'format': 'str', 'type': 'string'},
                'description': 'user login'
            },
            {
                'in': 'headers',
                'name': 'X-Password',
                'schema': {'format': 'str', 'type': 'string'},
                'description': 'user password'
            },
        ]
        responses:
            200:
                description: return user and set valid token in cookie
                schema:
                    $ref = "#/components/schemas/User"
            403:
                description: login or password invalid
    """

    IS_ENABLE_AUTH: bool = False

    def get_data(self,
                 req: Request,
                 resp: Response,
                 *,
                 id_user: Union[str, int]) -> None:
        """Get data."""
        token: str = req.cookies.get('token')
        password: str = req.headers.get('X-Password')

        if not token and not password:
            api.set_forbidden(req, resp)
            return

        # reject digital password
        if password:
            try:
                int(password)
            except ValueError:
                pass
            else:
                api.set_forbidden(req, resp)
                return

        user_query: Query = api.db_session.query(
            models.User
        )

        try:
            id_user = int(id_user)
        except ValueError:
            user: models.User = user_query.filter(
                models.User.login == id_user).first()
        else:
            user = user_query.get(id_user)

        if user is None:
            api.set_forbidden(req, resp)
            return

        if not api.user_auth(user=user, token=token, password=password):
            api.set_forbidden(req, resp)
            return

        if password:
            resp.cookies['token'] = '; '.join(
                [user.token, *settings.PARAMS_FOR_TOKEN_COOKIE])

        resp.media = UserSchema().dump(user)


@api.route('/api/v1/authwithgoogle')
class GoogleAuth(api.BaseViewed):
    r"""
    Get user information and set token in cookie if valid request.

    Access:
    header 'X-Google-Auth-Code'

    ---
    get:
        description: Get user information and set token \
                     in cookie if valid request
        parameters: [
            {
                'in': 'headers',
                'name': 'X-Google-Auth-Code',
                'schema': {'format': 'str', 'type': 'string'},
                'description': 'google auth code'
            },
        ]
        responses:
            200:
                description: return user and set valid token in cookie
                schema:
                    $ref = "#/components/schemas/User"
            403:
                description: request invalid
    """

    IS_ENABLE_AUTH: bool = False

    def get_data(self, req: Request, resp: Response) -> None:
        """Get data."""
        # csrf protect
        if not req.headers.get('X-Requested-With'):
            api.set_forbidden(req, resp)
            return

        auth_code: str = req.headers.get('X-Google-Auth-Code')

        try:
            credentials: client.OAuth2Credentials = (
                client.credentials_from_clientsecrets_and_code(
                    settings.GOOGLE_CLIENT_SECRET_FILE,
                    ['profile', 'email'],
                    auth_code
                )
            )
        except Exception:
            api.set_forbidden(req, resp)
            return

        userid: str = credentials.id_token['sub']
        email: str = credentials.id_token['email']

        try:
            user: models.User = api.db_session.query(
                models.User
            ).filter(
                models.User.login == email
            ).one()
        except NoResultFound:
            user = models.User(
                login=email, email=email, creation=datetime.now())
            user.create_password(str(userid))
            api.db_session.add(user)
            api.db_session.commit()

        resp.cookies['token'] = '; '.join(
            [user.token, *settings.PARAMS_FOR_TOKEN_COOKIE])

        resp.media = UserSchema().dump(user)
