from responder.models import Request, Response

from backend import models
from backend.app import api
from backend.schemas.movie import MovieSchema


@api.route('/api/v1/movies/')
class ListMovies(api.BaseViewed):
    r"""
    Get list all movies for user support filters on type and status.
    through relevant get params or Add (Post) one new movie.

    ---
    get:
        description: get list movies
        parameters: [
            {
                'in': 'path',
                'name': 'type',
                'schema': {'format': 'int32', 'type': 'integer'},
                'description': 'movie type (1 - serial, 2 - film)'
            },
            {
                'in': 'path',
                'name': 'status',
                'schema': {'format': 'int32', 'type': 'integer'},
                'description': 'movie status (\
                                1 - watched, 2 - watching, 3 - not watched)'
            },
        ]
        responses:
            200:
                description: return list movies current user
                schema:
                    $ref = "#/components/schemas/Movie"
            403:
                description: token in cookie invalid

    post:
        description: create new movie
        responses:
            201:
                description: return created movie
                schema:
                    $ref = "#/components/schemas/Movie"
            403:
                description: token in cookie invalid
    """

    def get_data(self, req: Request, resp: Response) -> None:
        """Get data."""
        status: str = req.params.get('status')
        type_movie: str = req.params.get('type')

        query_filters: tuple = (
            status is None or models.Movie.status == status,
            type_movie is None or models.Movie.type_movie == type_movie,
        )

        movies: list = api.db_session.query(
            models.Movie
        ).filter(
            models.Movie.id_user == self.ID_USER,
            *query_filters
        ).all()

        resp.media = MovieSchema(many=True).dump(movies)

    def post_data(self, req: Request, resp: Response, media: str) -> None:
        """Create data."""
        movie_data: dict = MovieSchema().load(media)
        movie_data['id_user'] = self.ID_USER

        movie: models.Movie = models.Movie(**movie_data)

        api.db_session.add(movie)
        api.db_session.flush()
        movie_id: int = movie.id
        api.db_session.commit()

        api.set_message_and_status(
            req, resp,
            message=movie_id,
            status_code=201,
            status=True
        )


@api.route('/api/v1/movies/{id_movie}')
class GetMovie(api.BaseViewed):
    """
    Get one movie for user.
    or
    Delete one movie for user
    or
    Edit (Put) one movie

    ---
    get:
        description: get movie
        responses:
            200:
                description: return one movie
                schema:
                    $ref = "#/components/schemas/Movie"
            403:
                description: token in cookie invalid
    delete:
        description: delete movie
        responses:
            200:
                description: success, return complete message
                schema:
                    $ref = "#/components/schemas/Movie"
            403:
                description: token in cookie invalid
    put:
        description: edit movie
        responses:
            200:
                description: success, movie edited
                schema:
                    $ref = "#/components/schemas/Movie"
            403:
                description: token in cookie invalid
    """

    def get_data(self, req: Request, resp: Response, *, id_movie: str) -> None:
        """Get data."""
        movie: models.Movie = api.db_session.query(
            models.Movie
        ).filter(
            models.Movie.id_user == self.ID_USER,
            models.Movie.id == id_movie,
        ).first()

        resp.media = MovieSchema().dump(movie)

    def delete_data(
            self, req: Request, resp: Response, *, id_movie: str) -> None:
        """Delete data."""
        movie: models.Movie = api.db_session.query(
            models.Movie
        ).filter(
            models.Movie.id_user == self.ID_USER,
            models.Movie.id == id_movie,
        )

        if movie.delete():
            api.db_session.commit()
            api.set_message_and_status(
                req, resp,
                message='Movie deleted successfully',
                status_code=200,
                status=True
            )

    def put_data(self,
                 req: Request,
                 resp: Response,
                 media: str,
                 *,
                 id_movie: str) -> None:
        """Edit data."""
        movie_data: dict = MovieSchema().load(media)

        movie: models.Movie = api.db_session.query(
            models.Movie
        ).filter(
            models.Movie.id_user == self.ID_USER,
            models.Movie.id == id_movie,
        )

        if not movie.update(movie_data):
            return

        api.db_session.commit()

        api.set_message_and_status(
            req, resp,
            message='Movie updated successfully',
            status_code=200,
            status=True
        )
