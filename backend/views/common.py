from functools import partial
from typing import Optional

import requests
from responder.models import Request, Response

from backend import settings
from backend.app import api


@api.route(default=True)
def msg_and_st(
        req: Request,
        resp: Response,
        message: str = 'Not found',
        status: bool = False,
        status_code: int = 404
) -> None:
    """
    Set status_code and body for response.
    In body returning message and status.

    :param status_code: http status code for response
    :param message: text for response body
    :param status: request status for response body (True|False)
    """
    resp.status_code = status_code
    resp.media = {'message': message, 'status': status}


api.set_message_and_status = msg_and_st
api.set_not_found = msg_and_st
api.set_forbidden = partial(
    msg_and_st, message='Not allowed', status=False, status_code=403
)
api.set_method_not_allowed = partial(
    msg_and_st, message='Method not allowed', status=False, status_code=405
)


class BaseViewed:
    """
    Base class for class-base viewed.

    Access:
        valid user id in header 'X-User' and valid token in cookie 'token'
    """

    IS_ENABLE_AUTH: bool = True
    ID_USER: Optional[int] = None

    def before_data_func(self, req: Request, resp: Response) -> None:
        """Call before {method}_data."""
        self.ID_USER = req.headers.get('X-User') or req.cookies.get('userId')

        # check auth
        if self.IS_ENABLE_AUTH and not api.user_auth(
                id_user=self.ID_USER,
                token=req.cookies.get('token'),
        ):
            api.set_forbidden(req, resp)
            return

        # set default status code and set none for resp.media
        resp.status_code = 200
        resp.media = None

    def after_data_func(self, req: Request, resp: Response) -> None:
        """Call after {method}_data."""
        api.db_session.close()

        # set not found if get_data not install media
        if not resp.media and not resp.content:
            api.set_not_found(req, resp)

    def on_request(self, req: Request, resp: Response, **kwargs) -> None:
        """Set not allowed status."""
        api.set_method_not_allowed(req, resp)

    def on_get(self, req: Request, resp: Response, **kwargs) -> None:
        """Get request."""
        self.before_data_func(req, resp)

        if resp.status_code != 200:
            return

        # call func for method
        self.get_data(req, resp, **kwargs)

        self.after_data_func(req, resp)

    def get_data(self, req: Request, resp: Response, **kwargs) -> None:
        """
        Process method get.
        Set 405 response for default.
        """
        api.set_method_not_allowed(req, resp)

    async def on_put(self, req: Request, resp: Response, **kwargs) -> None:
        """Put request."""
        self.before_data_func(req, resp)

        if resp.status_code != 200:
            return

        media: str = await req.media()

        # call func for method
        self.put_data(req, resp, media, **kwargs)

        self.after_data_func(req, resp)

    def put_data(
            self, req: Request, resp: Response, media: str, **kwargs) -> None:
        """
        Process method put.
        Set 405 response for default.
        """
        api.set_method_not_allowed(req, resp)

    async def on_post(self, req: Request, resp: Response, **kwargs) -> None:
        """Post request."""
        self.before_data_func(req, resp)

        if resp.status_code != 200:
            return

        media: str = await req.media()

        # call func for method
        self.post_data(req, resp, media, **kwargs)

        self.after_data_func(req, resp)

    def post_data(
            self, req: Request, resp: Response, media: str, **kwargs) -> None:
        """
        Process method post.
        Set 405 response for default.
        """
        api.set_method_not_allowed(req, resp)

    def on_delete(self, req: Request, resp: Response, **kwargs) -> None:
        """Delete request."""
        self.before_data_func(req, resp)

        if resp.status_code != 200:
            return

        # call func for method
        self.delete_data(req, resp, **kwargs)

        self.after_data_func(req, resp)

    def delete_data(self, req: Request, resp: Response, **kwargs) -> None:
        """
        Process method delete.
        Set 405 response for default.
        """
        api.set_method_not_allowed(req, resp)


api.BaseViewed = BaseViewed


@api.route('/version')
class GetVersion(api.BaseViewed):
    """
    Get app version.

    ---
    get:
        description: get current app version
        responses:
            200:
                description: return one movie
    """

    IS_ENABLE_AUTH: bool = False

    def get_data(self, req: Request, resp: Response) -> None:
        """Get data."""
        api.set_message_and_status(
            req, resp,
            message=api.version,
            status_code=200,
            status=True
        )


@api.route('/')
class IndexSPA(api.BaseViewed):
    """Return vue.js spa page."""

    IS_ENABLE_AUTH: bool = False

    def get_data(self, req: Request, resp: Response) -> None:
        """Get data."""
        if not settings.DEBUG:
            resp.content = api.template('index.html')
            return

        resp.content = requests.get(
            f'http://localhost:{settings.DEBUG_FRONT_PORT}/').text


@api.route('/_nuxt/{path}')
class DebugSPA(api.BaseViewed):
    """Return data from vue.js develop server."""

    IS_ENABLE_AUTH: bool = False

    def get_data(self, req: Request, resp: Response, *, path: str) -> None:
        """Get data."""
        if not settings.DEBUG:
            api.set_not_found(req, resp)
            return

        resp.content = requests.get(
            f'http://localhost:{settings.DEBUG_FRONT_PORT}/_nuxt/{path}').text


@api.route('/sw.js')
class ServiceWorker(api.BaseViewed):
    """Return service worker script for PWA."""

    IS_ENABLE_AUTH: bool = False

    def get_data(self, req: Request, resp: Response) -> None:
        """Get data."""
        resp.content = api.template('sw.js')
