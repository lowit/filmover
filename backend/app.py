import responder
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from backend import settings

# create application
api = responder.API(
    debug=settings.DEBUG,
    title=settings.TITLE,
    version=settings.VERSION,
    static_dir=settings.STATIC_DIR,
    static_route=settings.STATIC_ROUTE,
    templates_dir=settings.TEMPLATES_DIR,
    auto_escape=settings.AUTO_ESCAPE,
    secret_key=settings.SECRET_KEY,
    enable_hsts=settings.ENABLE_HSTS,
    cors=settings.CORS,
    cors_params=settings.CORS_PARAMS,
    allowed_hosts=settings.ALLOWED_HOSTS,
    docs_route=settings.DOCS_ROUTE,
    openapi=settings.OPENAPI
)

# connection to database
engine = create_engine(settings.DB_URL, **settings.DB_PARAMS)
Session = sessionmaker(bind=engine)
api.db_engine = engine
api.db_session = Session()

# creating base model object
api.DBModelBase = declarative_base()

# import utils
from backend.utils import *  # noqa

# import views
from backend.views.common import *  # noqa
from backend.views.movie import *  # noqa
from backend.views.user import *  # noqa

if __name__ == '__main__':
    api.run()
