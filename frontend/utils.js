/**
 * seconds to format hhmmss converter
 */
export function secondsToTimeConverter (seconds, sep = '') {
  let hours = Math.floor(seconds / 3600);
  let minutes = Math.floor((seconds - (hours * 3600)) / 60);
  let sec = seconds - (hours * 3600) - (minutes * 60);

  if (hours < 10) {hours = "0"+hours}
  if (minutes < 10) {minutes = "0"+minutes}
  if (sec < 10) {sec = "0"+sec}

  return `${hours}${sep}${minutes}${sep}${sec}`
}

/**
 * time format hhmmss to seconds converter
 *
 * @param time
 */
export function timeToSecondsConverter (time) {
  let _time = time.toString();
  let sec = parseInt(_time.slice(4, 6));
  let min = parseInt(_time.slice(2, 4))*60;
  let hr = parseInt(_time.slice(0, 2))*3600;
  return sec + min + hr
}
