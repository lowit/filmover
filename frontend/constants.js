// params
export const COOKIE_PARAMS = {path: '/', maxAge: 7948800}; // 92 days

// for ui
export const WATCHED_STATUS = 1;
export const WATCHING_STATUS = 2;
export const WATCH_STATUS = 3;
export const TAB_INDEX_MAP = {watched: 1, watching: 2, watch: 3};
export const DEFAULT_TAB = 'watching';

// for movie
export const SERIAL_TYPE = 1;
export const FILM_TYPE = 2;

// for user
