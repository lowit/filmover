import Vue from 'vue'

let lhost = location.host.split(':');
const PROTOCOL = location.protocol;
const HOST = lhost[0];
let PORT = lhost.length > 1 ? `:${lhost[1]}` : '';
const PREFIX = 'api/v1/';

// override port for develop
PORT = PORT === ':3000' ? ':5042' : PORT;

export function apiUrl(endpoint) {
  return `${PROTOCOL}//${HOST}${PORT}/${PREFIX}${endpoint}`
}

Vue.prototype.$apiUrl = apiUrl;
