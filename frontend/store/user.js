import { apiUrl } from '~/plugins/url-constructor'
import { COOKIE_PARAMS } from '~/constants'


export const state = () => ({
  user: null,
  isLogined: false
});

export const mutations = {
  setIsLogined (state, status) {
    state.isLogined = status
  },
  setUser (state, user) {
    state.user = user
  }
};

export const getters = {
  userId: state => {
    return state.user.id
  }
};

export const actions = {
  /**
   * get access code from google and sending on backend
   */
  async loginUserWithGoogle(context, code) {
    let user = await this.$axios.$get(
      apiUrl('authwithgoogle'), {
        headers: {'X-Google-Auth-Code': code,
                  'X-Requested-With': 'XMLHttpRequest'}
      }
    );
    await context.dispatch('saveUser', user);
  },

  async saveUser(context, user) {
    context.commit('setUser', user);
    this.$cookies.set('userId', user.id, COOKIE_PARAMS)
    context.commit('setIsLogined', true);
  },

  async loginUser(context, { userId, password }) {
    let headers = {};
    if (password !== undefined) {headers = {'X-Password': password}}
    let user = await this.$axios.$get(
      apiUrl(`users/${userId}`), { headers });
    await context.dispatch('saveUser', user);
  },
};
