import { TAB_INDEX_MAP } from '~/constants'
import { SERIAL_TYPE } from '~/constants'
import { FILM_TYPE } from '~/constants'
import { apiUrl } from '~/plugins/url-constructor'

export const state = () => ({
  serialsItems:
    {active: true, action: 'perm_media', title: 'Serials', items: []},
  filmsItems: {action: 'movie', title: 'Films', items: []}
});

export const mutations = {
  updateSerialsItems (state, serials) {
    state.serialsItems.items = serials
  },
  updateFilmsItems (state, films) {
    state.filmsItems.items = films
  }
};

export const actions = {
  /**
   * Get films and serials from backend and save in store
   */
  async getMovies(context, tab) {
    let moviesList = await this.$axios.$get(
      apiUrl('movies/'), {params: {status: TAB_INDEX_MAP[tab]}}
    ).catch((ex) => {
      return []
    });
    context.commit(
      'updateSerialsItems', moviesList.filter(
        film => film.type_movie === SERIAL_TYPE
      )
    );
    context.commit(
      'updateFilmsItems', moviesList.filter(
        film => film.type_movie === FILM_TYPE
      )
    )
  },

  /**
   * Remove one movie
   */
  async removeMovie(context, movie) {
    await this.$axios.$delete(apiUrl(`movies/${movie.id}`))
  },

  /**
   * Edit movie
   */
  async editMovie(context, { movie_id, movie_data }) {
    await this.$axios.$put(apiUrl(`movies/${movie_id}`), movie_data)
  },

  /**
   * Add movie
   */
  async addMovie(context, movie_data) {
    await this.$axios.$post(apiUrl('movies/'), movie_data)
  }
};
