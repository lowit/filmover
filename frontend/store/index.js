import { DEFAULT_TAB } from '~/constants'


export const strict = false;

export const state = () => ({
  currentTab: DEFAULT_TAB,
  isActiveMovieEditBar: false,
  isActiveEditMovie: false,
  currentEditMovie: null
});

export const mutations = {
  updateTabName(state, tabName) {
    state.currentTab = tabName
  },
  isActiveMovieEditBar(state, status) {
    state.isActiveMovieEditBar = status
  },
  setCurrentEditMovie(state, movie) {
    state.currentEditMovie = movie
  },
  isActiveEditMovie(state, status) {
    state.isActiveEditMovie = status
  }
};

export const actions = {
  /**
   * Update tab name in store
   */
  updateTabInStore (context, tabName) {
    context.commit('updateTabName', tabName)
  },

  /**
   * Update isActiveEditMovie name in store
   */
  isActiveEditMovie(context, status) {
    context.commit('isActiveEditMovie', status)
  },

  /**
   * Set status open edit bar
   */
  isActiveMovieEditBar(context, status) {
    context.commit('isActiveMovieEditBar', status);
  },

  /**
   * Set current edit movie
   */
  setCurrentEditMovie(context, movie) {
    context.commit('setCurrentEditMovie', movie);
  },

  /**
   * Open edit bar
   */
  openEditBar(context, movie) {
    context.commit('isActiveMovieEditBar', true);
    context.commit('setCurrentEditMovie', movie)
  },

  /**
   * Open add movie page
   */
  openAddMovie(context) {
        context.commit('isActiveEditMovie', true);
        context.commit('setCurrentEditMovie', null)
  }
};
