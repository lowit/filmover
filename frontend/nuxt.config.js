module.exports = {
  /*
  ** Build configuration
  */
  build: {},
  mode: 'spa',
  /*
  ** Headers
  ** Common headers are already provided by @nuxtjs/pwa preset
  */
  head: {
    script: [
      // load script for google sign-in
      { src: 'https://apis.google.com/js/client:platform.js?onload=start' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Customize app manifest
  */
  manifest: {
    theme_color: '#3B8070'
  },
  /*
  ** Modules
  */
  modules: [
    'nuxt-rfg-icon',
    '@nuxtjs/pwa',
    '@nuxtjs/vuetify',
    'cookie-universal-nuxt',
    '@nuxtjs/axios'
  ],
  vuetify: {
    // Vuetify options
    //  theme: { }
  },
 'rfg-icon': {
    static: true,
    staticPath: '/_nuxt/_favicons/',
    masterPicture: 'static/icon.png',
  },
  axios: {
    credentials: true,
  },
  plugins: [
    '~/plugins/url-constructor.js'
  ]
};
