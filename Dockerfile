FROM python:3.6-alpine

EXPOSE 5042/tcp

WORKDIR /app

COPY Pipfile /app
COPY Pipfile.lock /app

RUN apk add gcc musl-dev make postgresql-dev python3-dev

RUN pip install pipenv && \
    pipenv install --deploy --system

RUN apk del gcc musl-dev make

COPY alembic.ini /app
COPY migrations /app/migrations

COPY backend /app/backend
COPY VERSION /app
COPY frontend/dist /app/frontend/dist

CMD export PYTHONPATH='.' && \
    alembic -x data=True upgrade head && \
    python backend/app.py
