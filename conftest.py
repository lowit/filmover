import os

import pytest

# Environ config
os.environ['PROFILE'] = 'test'
os.environ['PYTHONPATH'] = '.'


def clear_db(_api):
    """Clear db."""
    api = _api

    def wrapper():
        for table in api.DBModelBase.metadata.tables:
            api.db_session.execute(f'truncate {table} cascade;')
            api.db_session.commit()

    return wrapper


@pytest.fixture(scope='function')
def api(request):
    """Create and return api object."""
    from backend import app
    api = app.api
    api.DBModelBase.metadata.create_all(api.db_engine)
    api.db_session.commit()
    return api


@pytest.fixture(scope='function')
def mixer(request, api):
    """
    Create and return mixer object.

    :param api: fixture api object
    """
    from backend.tests.fixtures.mixer import Xyuxer
    _mixer = Xyuxer(session=api.db_session, commit=True)
    request.addfinalizer(clear_db(api))
    return _mixer


# import default fixtures
from backend.tests.fixtures.default_fixtures import *  # noqa
