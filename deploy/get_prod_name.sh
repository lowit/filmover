#!/usr/bin/env bash

curl -sI -X GET https://filmover.ru/ | grep X-WHO-IS-HOME | awk '{print $2}' | cut -c 1-4
