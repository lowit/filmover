#!/usr/bin/env bash

PROD=$(deploy/get_prod_name.sh)
CAIN="cain"
ABEL="abel"

if [[ "${PROD}" == "$CAIN" ]]; then
    echo ${ABEL}
fi

if [[ "${PROD}" == "$ABEL" ]]; then
    echo ${CAIN}
fi
