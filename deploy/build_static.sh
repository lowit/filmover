#!/usr/bin/env bash

# workaround
# set filmover version in package.json
PACKG=$(cat frontend/package.json)
echo ${PACKG} | sed 's/"version": "0.0.0",/"version": "'${VERSION}'",/' > frontend/package.json

# build static
cd frontend && npm i && npm run build && cd ..

# revert package.json
echo ${PACKG} > frontend/package.json
