"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""
from alembic import op, context
import sqlalchemy as sa
${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}


def upgrade():
    schema_upgrades()
    if context.get_x_argument(as_dictionary=True).get('data'):
        data_upgrades()


def downgrade():
    if context.get_x_argument(as_dictionary=True).get('data'):
        data_downgrades()
    schema_downgrades()


def schema_upgrades():
    ${upgrades if upgrades else "pass"}


def schema_downgrades():
    ${downgrades if downgrades else "pass"}


def data_upgrades():
    pass


def data_downgrades():
    pass
