"""init schema db

Revision ID: 164e4148aee1
Revises: 
Create Date: 2018-12-11 00:17:59.420206

"""
import sqlalchemy as sa
from alembic import op, context

# revision identifiers, used by Alembic.
revision = '164e4148aee1'
down_revision = None
branch_labels = None


def upgrade():
    schema_upgrades()
    if context.get_x_argument(as_dictionary=True).get('data'):
        data_upgrades()


def downgrade():
    if context.get_x_argument(as_dictionary=True).get('data'):
        data_downgrades()
    schema_downgrades()


def schema_upgrades():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('login', sa.String(64), nullable=False, unique=True),
        sa.Column('password', sa.String(1024), nullable=False),
        sa.Column('email', sa.String(128), nullable=False, unique=True),
        sa.Column(
            'creation',
            sa.TIMESTAMP,
            server_default=sa.text('CURRENT_TIMESTAMP'),
        ),
    )

    op.create_table(
        'movies',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column(
            'id_user', sa.Integer, sa.ForeignKey('users.id'), nullable=False),
        sa.Column('name', sa.String(256), nullable=False),
        sa.Column('year', sa.Integer),
        sa.Column('rating', sa.Float),
        sa.Column('tags', sa.JSON),
        sa.Column('type_movie', sa.SmallInteger, nullable=False),
        sa.Column('status', sa.SmallInteger, nullable=False),
        sa.Column('season_count', sa.SmallInteger),
        sa.Column('season_current', sa.SmallInteger),
        sa.Column('episodes_per_season', sa.SmallInteger),
        sa.Column('episode_current', sa.SmallInteger),
        sa.Column('time_current', sa.Integer),
    )

    op.create_index(op.f('status'), 'movies', ['status'], unique=False)
    op.create_index(op.f('type_movie'), 'movies', ['type_movie'], unique=False)


def schema_downgrades():
    op.drop_table('movies')
    op.drop_table('users')


def data_upgrades():
    pass


def data_downgrades():
    pass
