from __future__ import with_statement

import os
import sys
from logging.config import fileConfig

from alembic import context
from sqlalchemy import create_engine

config = context.config

fileConfig(config.config_file_name)

target_metadata = None

STAGE_PROFILE_MAP = {
    'devel': 'local',
    'prod': 'release',
}

sys.path.append(os.getcwd())

# if parameter -n not set raise KeyError
try:
    os.environ['PROFILE'] = STAGE_PROFILE_MAP[config.get_main_option('stage')]
except KeyError:
    if os.environ['PROFILE'] not in STAGE_PROFILE_MAP.values():
        AssertionError('PROFILE not set')

from backend import settings


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    context.configure(
        url=settings.DB_URL,
        target_metadata=target_metadata,
        literal_binds=True
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = create_engine(settings.DB_URL)

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
