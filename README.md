# filmover

[![pipeline status](https://gitlab.com/lowit/filmover/badges/master/pipeline.svg)](https://gitlab.com/lowit/filmover/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/lowit/filmover/badge.svg?branch=HEAD)](https://coveralls.io/gitlab/lowit/filmover?branch=HEAD)

Web application for memories viewed films/serials.
Hosted on [filmover.ru](https://filmover.ru)

## Run migration:

    alembic --name devel -x data=True upgrade head


## Testing:

1. Run container with database;

    ```
    docker-compose run -d -p5432:5432 filmover-db
    ```

2. Create test database and permission:

    ```
    create database test_filmover;
    create user test_filmover with encrypted password 'test_filmover';
    grant all privileges on database test_filmover to test_filmover;
    ```
3. Run tests:

    ```
    pytest backend
    ```
